C++ Label Merge
===============

This repo contains a **C++ implementation** of a **custom-designed algorithm**
for **merging connected labels** on digital images.

(In this context, the term **"connected label"** refers to a
[Connected-Component Label](https://en.wikipedia.org/wiki/Connected-component_labeling)
in digital image processing or computer vision.
Connected labels are generally produced as a result of
[image segmentation](https://en.wikipedia.org/wiki/Image_segmentation)
-- and often also as an intermediate processing step during the task.)

This C++ code is my implementation of an algorithm
that merges connected labels that overlap in quantized colour.
For example, if there are 2 or more labels of different label-value,
but they overlap in quantized colour, those labels will be merged.

This is some self-contained C++ code that I wrote, to implement an algorithm
that I designed, during my work in digital image processing & computer vision.

What's in this repo?
--------------------

The code for this algorithm is in the `compiledpp/` subdirectory:

- the C++ code
- a Makefile (to compile the C++ code)
- a small Python2 `__init__.py` wrapper-module (to define a Python sub-package)

Additionally, in this top-level directory:

- a small Python2 test script
- a test image
  [`SIPI_Jelly_Beans_4.1.07.png`](https://en.wikipedia.org/wiki/File:SIPI_Jelly_Beans_4.1.07.tiff)
  for use by the Python2 test script

The C++ code is also a Python2 C-API module
-------------------------------------------

This C++ code also implements the
[Python2 C-API](https://docs.python.org/2/c-api/index.html).
As a result, when this C++ code is compiled to a shared library (`.so` suffix),
this shared library is a
[Python2 extension module](https://docs.python.org/2/extending/index.html)
that may be imported & used in Python2 code like any other Python2 module.

When this C++ code is compiled:

- it produces a shared library `_quantcolourlabelmerge.so`
- that can be imported as a Python2 module `_quantcolourlabelmerge`
- that contains a single Python2 function `merge_by_quant_colour`.

(Sorry about the long name, but I've designed & implemented more than a few
label-merging algorithms in my time.  I've learned that it's better to be
descriptive when naming them...)

In fact, this C++ code also implements the
[NumPy C-API](https://numpy.org/doc/stable/reference/c-api/index.html),
which means that this C++ code is able to take its input, and return its
result, as
[native NumPy arrays](https://numpy.org/doc/stable/reference/c-api/array.html).

As a general approach,
this enables us to prototype our high-level algorithms in Python,
[NumPy](https://numpy.org/),
[SciPy](https://scipy.org/),
[OpenCV](https://opencv.org/)
(as [OpenCV-Python](https://pypi.org/project/opencv-python/));
then write high-performance C++ code to
[implement our own novel algorithms in compiled C++](https://numpy.org/doc/stable/user/c-info.html)
(for algorithms that aren't already implemented in NumPy, SciPy, or OpenCV).

Python2 wrapper module `compiledpp/__init__.py`
-----------------------------------------------

In the `compiledpp/` subdirectory, there's also a small Python2 `__init__.py`
wrapper-module.  This `compiledpp/__init__.py` file means that Python will
treat the contents of subdirectory `compiledpp/` as a Python sub-package.

This means that, outside the `compiledpp/` subdirectory,
you can write Python code like:

```
import compiledpp
```

or this:

```
from compiledpp import _quantcolourlabelmerge
```

or even this:

```
from compiledpp._quantcolourlabelmerge import merge_by_quant_colour
```

Python2 test-script
-------------------

In the top-level directory (outside the `compiledpp/` subdirectory),
there's a tiny Python2 test-script to demo our compiled C++
Python2 C-API module.

To use this test-script, run the following in the top-level directory:

```
python test_compiledpp.py
```

What this tiny Python2 test-script does:

 1. Load the accompanying test image `SIPI_Jelly_Beans_4.1.07.png`.
 2. Label the "smooth regions" in the image (performing the world's simplest
    [image segmentation](https://en.wikipedia.org/wiki/Image_segmentation)).
 3. Quantize the colours in the image (performing the world's simplest
    [colour quantization](https://en.wikipedia.org/wiki/Color_quantization)).
 4. Use our fancy home-grown label-merging algorithm implemented in C++
    (called via function `merge_by_quant_colour` in package `compiledpp`)
    to merge by labels & quantized colours.
 5. Plot the results using Matplotlib.

![Screenshot of the Matplotlib window shown by `test_compiledpp.py`](https://gitlab.com/jboyden/cpp-label-merge/-/raw/main/Screenshot_of_test_compiledpp.png "Screenshot of the Matplotlib window shown by `test_compiledpp.py`")

To compile the C++ code
-----------------------

The C++ code (in subdirectory `compiledpp/`) should be compiled as
a Python2 C-API module, using the supplied `compiledpp/Makefile`:

```
$ ( cd compiledpp/ && make )
```

This should produce a compilation command that looks like:

```
$ ( cd compiledpp/ && make )
g++ -shared -o _quantcolourlabelmerge.so quantcolourlabelmerge.cc qclm_algo.cc PyArrayPP.cc -Wall -Wextra -O3 -fPIC -std=c++11 -Wconversion -Wdouble-promotion  -lpython2.7 -I /usr/include/python2.7 -I /usr/local/lib/python2.7/dist-packages/numpy/core/include -D NPY_NO_DEPRECATED_API=NPY_1_7_API_VERSION
```

This will create a new file `compiledpp/_quantcolourlabelmerge.so`.

C++ files in subdirectory `compiledpp/`
---------------------------------------

- `quantcolourlabelmerge.cc`:
  Quantized Colour Label Merge, Python2 entry-point via
  [Python2 C-API](https://docs.python.org/2/c-api/index.html).
- `qclm_algo.hh` & `qclm_algo.cc`:
  Quantized Colour Label Merge, algorithm implementation.
- `PyArrayPP.hh` & `PyArrayPP.cc`:
  Light-weight C++ wrappers around already-validated
  [Numpy arrays](https://numpy.org/doc/stable/reference/c-api/array.html).

Package dependencies (Ubuntu Linux / Linux Mint)
------------------------------------------------

If you just want to compile the C++:

- `g++`
- `make`
- `python-dev`
- `python-numpy-dev`

And if you want to run the Python2 test-script, you also need:

- `python`
- `python-matplotlib`
- `python-numpy`
- `python-scipy`

