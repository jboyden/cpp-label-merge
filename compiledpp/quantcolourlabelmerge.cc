/*
 * quantcolourlabelmerge.cc (Quantized Colour Label Merge, Python entry-point):
 * Merge region labels that overlap in quantized colour.
 *
 * We assume that each region label is 8-connected; but the quantized colours
 * are NOT 8-connected, because they might be spread across the image.
 *
 * This module enables us to merge differently-labeled region labels,
 * discontiguous across the image, if they overlap in quantized colour.
 * It builds a bidirectional graph of (region label <--> quantized colour),
 * then merges labels that are joined by graph edges.  It's pretty fancy.
 *
 * This C++ source file contains the Python C-API wrapper code,
 * and provides the entry point from Python.
 *
 * This C++ source file depends upon the files `PyArrayPP.hh`, `PyArrayPP.cc`,
 * `qclm_algo.hh`, and `qclm_algo.cc`.  Compile it all using the Makefile.
 */

#include <sstream>  // std::ostringstream

#include <Python.h>
#include "numpy/arrayobject.h"
#include "numpy/npy_common.h"

#include "PyArrayPP.hh"
#include "qclm_algo.hh"

/*
 * The all-capitals macro `NPY_INT32` is listed here:
 *  https://docs.scipy.org/doc/numpy/reference/c-api.dtype.html#enumerated-types
 *
 * On the topic of types in Numpy (in Python and C), here is some fun reading:
 *  https://docs.scipy.org/doc/numpy/user/basics.types.html
 *  https://docs.scipy.org/doc/numpy/reference/arrays.dtypes.html
 *  https://docs.scipy.org/doc/numpy/reference/c-api.dtype.html
 */


/******************************************************************************
 *
 * Function prototype declarations.
 *
 */


/*
 * Verify the supplied PyArrayObject array argument,
 * which is expected to be a 2-D array of Numpy data-type `int32` or `uint32`.
 *
 * Upon success, return a new `PyArrayPP:PyArray2d<int32_t>` instance.
 * Upon failure, throw a `PyArrayPP:ValueErrorForParam` exception.
 */
static
PyArrayPP::PyArray2d<int32_t>
verify_py_array_arg(
        PyArrayObject *py_arr,
        const char *param_name,
        const char *param_descr,
        const PyArrayPP::Shape2d &expected_shape);


/*
 * Verify that array element values are within range at every pixel.
 *
 * Upon failure, throw a `PyArrayPP:ValueErrorForParam` exception.
 */
static
void
verify_range_at_each_pixel(
        const PyArrayPP::PyArray2d<int32_t> &arr_2d,
        const char *param_name,
        const char *param_descr,
        int32_t expected_min_val,
        int32_t expected_max_val);




/******************************************************************************
 *
 * Function `py_merge_by_quant_colour` is the entry-point for this module.
 *
 */


/*
 * Specification:
 *   merge_by_quant_colour(
 *          labels_to_merge, num_labels_to_merge,
 *          quant_colours, num_quant_colours,
 *          merged_labels_out,
 *          do_checks=True)
 *
 * Return value:
 *   num_merged_labels
 *
 * We specificaly require these arrays to have dtype of `int32` or `uint32`:
 * - `labels_to_merge`
 * - `quant_colours`
 * - `merged_labels_out`
 *
 * All 3 of these int32-element-size argument arrays must have the same shape.
 *
 * For arrays `labels_to_merge`, `quant_colours`, and `merged_labels_out`,
 * this is because we will be merging the values in these arrays by overlap;
 * so the arrays must overlap exactly!
 *
 * It is fine for `labels_to_merge` & `merged_labels_out` to actually be
 * the same array; this will effectively overwrite the input labels array
 * with output labels, in-place.
 *
 *
 * As a general coding practice, we require that our labels arrays use
 * at least a 32-bit dtype according to the following reasoning:
 *
 * 1. We currently use 8-connected labels.  This means that there must be
 *    at least 1 pixel separating all nearby labels, in all 8 directions.
 *    This means the highest possible number of labels to cover an image
 *    (the densest-possible arrangement) would be this 1-in-4 pattern,
 *    repeated to cover the image:
 *      + -
 *      - -
 *
 * 2. This means the highest possible number of labels is:
 *      `(num_rows // 2) * (num_cols // 2)`
 *    (ignoring off-by-one errors due to odd side-lengths), or equivalently:
 *      `(num_rows * num_cols) // 4`
 *
 * 3. Thus, even a moderately-small image shape of (800, 800) could potentially
 *    yield up to `800 * 800 // 4 == 160000` 8-connected labels.
 *
 * 4. This exceeds 65535 (`(1 << 16) - 1`), which is the largest value that can
 *    be stored in a 16-bit integer (even an unsigned 16-bit integer, in fact).
 *    Thus, we need to use at least a 32-bit dtype for our labels arrays.
 *
 * 5. Aside:  Using 4-connected labels rather than 8-connected labels would
 *    actually *increase* the need for a 32-bit labels array dtype, because
 *    it would *increase* the density of the densest-possible arrangement to
 *    a 2-in-4 chessboard pattern:
 *      + -
 *      - +
 *    which would change the highest possible number of labels to:
 *      `(num_rows * num_cols) // 2`
 *    which would mean that an image of shape (800, 800) could now potentially
 *    yield up to `800 * 800 // 2 == 320000` 4-connected labels, which is twice
 *    the previous count, and almost 5x the highest value that can be stored in
 *    a 16-bit integer (even unsigned).
 *
 * 6. At the other end of the scale, if we allow a theoretical maximum image
 *    shape of (20000, 20000) with 8-connected labels, that could yield up to
 *    `20_000 * 20_000 // 4 == 100_000_000` (100 million) labels, which would
 *    still fit easily within the 2_147_483_647 (2 thousand million) positive
 *    values of a signed 32-bit integer.
 */
static PyObject *
py_merge_by_quant_colour(PyObject *class_, PyObject *args, PyObject *kwargs)
{
    /*
     * This function is ~140 lines of code just to parse the C-API args
     * and check the PyObject types (and the expected PyArrayObject shapes,
     * dtypes, C-contiguity, and value ranges) before we can finally extract
     * the PyArray "data" (the underlying dynamically-allocated arrays) in C++.
     *
     * The actual algorithm is implemented by this non-C-API subroutine:
     *  - function `PyArrayPP::merge_labels`
     * defined in source file "qclm_algo.cc".
     */

    PyArrayObject *py_array_labels_to_merge = NULL,
                  *py_array_quant_colours = NULL,
                  *py_array_merged_labels_out = NULL;
    int num_labels_to_merge = 0, num_quant_colours = 0;
    int do_checks = 1;  // Default to True: do checks

    /*
     * Parse the C-API args and check the PyObject types.
     * This `kwlist` is for use with `PyArg_ParseTupleAndKeywords`.
     */

    static const char *kwlist[] = {
            "labels_to_merge", "num_labels_to_merge",
            "quant_colours", "num_quant_colours",
            "merged_labels_out",
            "do_checks",
            NULL };

    // If a parsing error occurs, C-API function `PyArg_ParseTupleAndKeywords`
    // raises an appropriate exception all by itself (so we don't need to):
    //  https://docs.python.org/3/c-api/arg.html#c.PyArg_ParseTupleAndKeywords
    if ( ! PyArg_ParseTupleAndKeywords(args, kwargs,
            // "O!" requires that the PyObject is of the specified type.
            "O!iO!iO!|i",
            // This `const_cast<char**>` is gross,
            // but it's just how the Python C-API is... :(
            const_cast<char**>(kwlist),
            &PyArray_Type, &py_array_labels_to_merge, &num_labels_to_merge,
            &PyArray_Type, &py_array_quant_colours,   &num_quant_colours,
            &PyArray_Type, &py_array_merged_labels_out,
            &do_checks)) {
        return NULL;
    }
    // Silence a warning about "unused parameter `class_` [-Wunused-parameter]".
    (void)class_;

    // Reduce error-handling/error-reporting boilerplate by raising our
    // own C++ exception `PyArrayPP:ValueError` whenever there's a problem.
    try {
        PyArrayPP::PyArray2d<int32_t> labels_to_merge_2d =
                verify_py_array_arg(
                        py_array_labels_to_merge,
                        // human-readable parameter name for error-reporting
                        "labels_to_merge",
                        // human-readable parameter descr for error-reporting
                        "input labels array",
                        // expected 2-D shape
                        PyArrayPP::Shape2d::NoShape  // no expected shape
                );

        PyArrayPP::PyArray2d<int32_t> quant_colours_2d =
                verify_py_array_arg(
                        py_array_quant_colours,
                        "quant_colours",
                        "input quantized colours",
                        labels_to_merge_2d.m_shape  // expect same shape as prev
                );

        PyArrayPP::PyArray2d<int32_t> merged_labels_out_2d =
                verify_py_array_arg(
                        py_array_merged_labels_out,
                        "output labels array",
                        "merged_labels_out",
                        labels_to_merge_2d.m_shape  // expect same shape as prev
                );

        // Now verify that all the array parameters are different
        // (except possibly for `labels_to_merge` & `merged_labels_out`,
        // which are allowed to be the same array).
        //
        // To be extra-careful, we will compare the memory addresses of the
        // underlying "data" arrays, rather than just the PyArray objects.

        if (labels_to_merge_2d.m_data == quant_colours_2d.m_data) {
            throw PyArrayPP::ValueError(
                    "`labels_to_merge` is same array as `quant_colours`");
        }
        if (quant_colours_2d.m_data == merged_labels_out_2d.m_data) {
            throw PyArrayPP::ValueError(
                    "`quant_colours` is same array as `merged_labels_out`");
        }

        // Verify that the caller didn't supply any negative counts
        // (which would be nonsensical).
        if (num_labels_to_merge < 0) {
            throw PyArrayPP::ValueError(
                    "num_labels_to_merge < 0");
        }
        if (num_quant_colours < 0) {
            throw PyArrayPP::ValueError(
                    "num_quant_colours < 0");
        }

        // Perform expensive range checks, if the function caller requests it...
        if (do_checks) {
            verify_range_at_each_pixel(
                    labels_to_merge_2d,
                    // human-readable parameter name for error-reporting
                    "labels_to_merge",
                    // human-readable parameter descr for error-reporting
                    "input labels array",
                    // lower bound of expected range
                    0,
                    // upper bound of expected range
                    num_labels_to_merge
            );
            verify_range_at_each_pixel(
                    quant_colours_2d,
                    "labels_to_merge",
                    "input labels array",
                    0,
                    num_quant_colours
            );
            // No need to verify the range in `merged_labels_out`,
            // which we treat as an uninitialised array.
        }

        // Now finally we can *use* the input labels arrays...
        long int num_merged_labels =
                QCLM::merge_labels(
                        merged_labels_out_2d,
                        labels_to_merge_2d, num_labels_to_merge,
                        quant_colours_2d, num_quant_colours);

        // PyObject* PyInt_FromLong(long ival):
        //  https://python.readthedocs.io/en/v2.7.2/c-api/int.html
        return PyInt_FromLong(num_merged_labels);

    } catch (const PyArrayPP::ValueError &e) {
        PyErr_SetString(PyExc_ValueError, e.what());
    }
    return NULL;
}




/******************************************************************************
 *
 * Verify caller-supplied array arguments.
 *
 */


/*
 * Verify the supplied PyArrayObject array argument,
 * which is expected to be a 2-D array of Numpy data-type `int32` or `uint32`.
 *
 * Upon success, return a new `PyArray2d<int32_t>` instance.
 * Upon failure, throw a `ValueErrorForParam` exception.
 */
static
PyArrayPP::PyArray2d<int32_t>
verify_py_array_arg(
        PyArrayObject *py_arr,
        const char *param_name,
        const char *param_descr,
        const PyArrayPP::Shape2d &expected_shape)
{
    // Step 1: Verify the expected `ndim` (number of dimensions).
    const int expected_ndim = 2;
    int actual_ndim = PyArray_NDIM(py_arr);
    if (actual_ndim != expected_ndim) {
        // Uh-oh!
        //
        // By the way, this code was written before `std::format` was available:
        //  https://en.cppreference.com/w/cpp/utility/format/format
        //
        // Of course, I would rather be using `std::format`...
        std::ostringstream oss;
        oss << "array ndim ("
                << actual_ndim
                << ") does not match expected ndim ("
                << expected_ndim
                << ")";

        throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                oss.str());
    }

    // We've verified that `ndim` == 2, so the shape has both rows & columns.
    // (But note that Numpy stores them as signed integers of type `npy_intp`.)
    npy_intp *py_array_shape = PyArray_DIMS(py_arr);
    npy_intp actual_num_rows = py_array_shape[0];
    npy_intp actual_num_cols = py_array_shape[1];

    // Step 2: Verify that signed integers `actual_num_rows` & `actual_num_cols`
    // are both positive integers.
    if (actual_num_rows <= 0 || actual_num_cols <= 0) {
        // Uh-oh!
        std::ostringstream oss;
        oss << "array shape ("
                << actual_num_rows
                << ", "
                << actual_num_cols
                << ") is not a valid shape";

        throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                oss.str());
    }
    // We've verified that signed integers `actual_num_rows` & `actual_num_cols`
    // are both positive integers.  So we can convert them from signed types to
    // unsigned types without changing the integer value.
    //
    // Convert from `npy_intp {aka long int}` -> `size_t`.
    PyArrayPP::Shape2d shape = PyArrayPP::Shape2d::make_valid(
            static_cast<size_t>(actual_num_rows),
            static_cast<size_t>(actual_num_cols));

    // Step 3: Do we need to verify the expected shape?
    if (expected_shape != PyArrayPP::Shape2d::NoShape) {
        // Yes, we need to verify that the shape matches an expected shape.
        if (expected_shape != shape) {
            // Uh-oh!
            std::ostringstream oss;
            oss << "array shape "
                    << shape
                    << " does not match expected shape "
                    << expected_shape;

            throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                    oss.str());
        }
    }

    // Step 4: Verify that the array thinks it is C-contiguous & aligned.
    if ( ! PyArray_IS_C_CONTIGUOUS(py_arr)) {
        // Uh-oh!
        throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                "array is not C-contiguous");
    }
    if ( ! PyArray_ISALIGNED(py_arr)) {
        // Uh-oh!
        throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                "array is not aligned");
    }

    // Step 5: Verify the expected element size, and C-contiguity, by stride.
    //
    // Note:  We care less about what `dtype` the Numpy array thinks it has,
    // and more about the actual element size, C-contiguity, and stride,
    // because we care most-of-all about array bounds and memory alignment.
    //
    // Don't forget to convert the positive size-in-bytes produced by `sizeof`
    // (a `size_t {aka long unsigned int}`) to the `npy_intp {aka long int}`
    // used for sizes by Numpy, to ensure a comparison of same-signedness.
    const npy_intp expected_elem_size = static_cast<npy_intp>(sizeof(int32_t));

    // Observe that we calculate this `actual_elem_size` using the stride.
    // Technically, the stride can be negative (if it's stepping backwards
    // because the array is reversed).
    //
    // So even though we generally think of "data type size" (in bits) using
    // `size_t {aka long unsigned int}` (and this variable's value will indeed
    // be compared against a `size_t` value), we'll declare this variable as
    // `npy_intp {aka long int}` to preserve the sign, because it's possible
    // that stepping backwards through the array might break something in our
    // implementation.
    npy_intp actual_elem_size = PyArray_STRIDE(py_arr, 1);
    if (actual_elem_size != expected_elem_size) {
        // Uh-oh!
        std::ostringstream oss;
        oss << "array elem size ("
                << actual_elem_size
                << " bytes) does not match expected elem size ("
                << expected_elem_size
                << " bytes)";

        throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                oss.str());
    }
    if (PyArray_STRIDE(py_arr, 0) != actual_num_cols * actual_elem_size) {
        // Uh-oh!
        throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                "array is not C-contiguous (by `strides[1]`)");
    }

    // `PyArray_DATA()` returns `void*`.  ("Hello void-star my old friend...")
    // In C, you can convert `void*` -> `int32_t*` implicitly.
    // In C++, you cannot.
    int32_t *data = static_cast<int32_t*>(PyArray_DATA(py_arr));

    return PyArrayPP::PyArray2d<int32_t>(data, shape);
}


/*
 * Verify that array element values are within range at every pixel.
 *
 * Upon failure, throw a `ValueErrorForParam` exception.
 */
static
void
verify_range_at_each_pixel(
        const PyArrayPP::PyArray2d<int32_t> &arr_2d,  // #include <starwars>?
        const char *param_name,
        const char *param_descr,
        int32_t expected_min_val,
        int32_t expected_max_val)
{
    const size_t num_pixels = arr_2d.num_elems();
    const int32_t *data = arr_2d.m_data;
    const int32_t *data_end = data + num_pixels;

    for ( ; data < data_end; ++data) {
        int32_t elem_val = *data;
        if (elem_val < expected_min_val || elem_val > expected_max_val) {
            // Uh-oh!
            // Now since we're going to report an error,
            // why don't we helpfully report the (row, col) index?
            size_t pixel_idx = data - arr_2d.m_data;
            size_t num_cols = arr_2d.m_shape.m_num_cols;
            size_t row_idx = pixel_idx / num_cols;
            size_t col_idx = pixel_idx % num_cols;

            std::ostringstream oss;
            oss << "array element value at index ("
                    << row_idx
                    << ", "
                    << col_idx
                    << ") is outside valid range ["
                    << expected_min_val
                    << ", "
                    << expected_max_val
                    << "]: "
                    << elem_val;

            throw PyArrayPP::ValueErrorForParam(param_name, param_descr,
                    oss.str());
        }
    }
}




/******************************************************************************
 *
 * Python C-API plumbling.
 *
 */


static PyMethodDef methods[] = {
	{ "merge_by_quant_colour", (PyCFunction) py_merge_by_quant_colour, METH_VARARGS | METH_KEYWORDS,
			"Merge region labels that overlap in quantized colour." },
    { NULL, NULL, 0, NULL },
};


PyMODINIT_FUNC
init_quantcolourlabelmerge(void)
{
    PyObject *m = Py_InitModule("_quantcolourlabelmerge", methods);
    if (m == NULL) {
        // Exit early if module init failed -- and also avoid "unused variable" warnings.
        return;
    }
    // Load `numpy` functionality.
    import_array();
}   


/*  
 * Vim modeline to expand tab characters to 4 spaces.
 * vim: et ts=4 sw=4
 */
