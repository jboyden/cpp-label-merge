/*
 * qclm_algo.hh (Quantized Colour Label Merge, algorithm implementation):
 * Merge region labels that overlap in quantized colour.
 *
 * This C++ header file is included by the C++ source files `qclm_algo.cc` &
 * `quantcolourlabelmerge.cc`.
 */

#ifndef QCLM_ALGO_HH
#define QCLM_ALGO_HH

#include <cstdint>  /* int32_t */

#include "PyArrayPP.hh"

namespace QCLM
{
    /*
     * Merge region labels that overlap in quantized colour.
     *
     * The merged region labels will be written into the pre-allocated
     * destination labels-array argument to parameter `merged_labels_out`.
     *
     * The number of merged region labels will be returned, an integer.
     * This will be a non-negative number: 0, 1, 2, 3, etc.
     * (Note that a return-value of 0 is entirely valid,
     * if there were no region labels to be merged, because `labels_to_merge`
     * and/or `quant_colours` consisted entirely of 0-valued labels/colours.)
     *
     * This function uses temporary vectors internally as RAII arrays,
     * then releases those vectors when the function is complete.
     */
    extern size_t
    merge_labels(
            PyArrayPP::PyArray2d<int32_t> &merged_labels_out_2d,
            const PyArrayPP::PyArray2d<int32_t> &labels_to_merge_2d,
            size_t num_labels_to_merge,
            const PyArrayPP::PyArray2d<int32_t> &quant_colours_2d,
            size_t num_quant_colours);

} // namespace QCLM

#endif // QCLM_ALGO_HH

/*  
 * Vim modeline to expand tab characters to 4 spaces.
 * vim: et ts=4 sw=4
 */
