/*
 * PyArrayPP.hh:
 * Light-weight C++ wrappers around already-validated Numpy arrays.
 *
 * [Note: These are minimalist, lightweight C++ wrappers around Numpy arrays
 * via the Python C-API.  If you want something a little more comprehensive,
 * take a look at: https://github.com/jboy/nim-pymod ]
 *
 * This C++ header file is included by the C++ header file `PyArrayPP.cc`.
 */

#ifndef PY_ARRAY_PP_HH
#define PY_ARRAY_PP_HH

#include <exception>  // std::exception
#include <iosfwd>  // forward-declaration of std::ostream
#include <string>  // std::string


namespace PyArrayPP
{
    /*
     * This class is a C++ exception that is thrown in this C++ code,
     * to be converted to a Python `ValueError` exception and raised
     * in the Python code.
     */
    class ValueError: std::exception
    {
    public:
        explicit
        ValueError(
                const char *err_descr):
            m_what(err_descr) {  }

        explicit
        ValueError(
                const std::string &err_descr):
            m_what(err_descr) {  }

        virtual
        ~ValueError() {  }

        // The C++ standard specifies that `std::exception::what()` returns
        // a null-terminated `const char *`, _NOT_ a `std::string`:
        //  https://en.cppreference.com/w/cpp/error/exception/what
        virtual
        const char *
        what() const noexcept
        {
            return m_what.c_str();
        }

        // This data member contains the error string that will be returned
        // by `std::exception::what()`.
        //
        // Question #1: Why do we store the error string as a data member?
        // Could we instead construct a new string in the `what()` method?
        //
        // Question #2: OK, then why is the data member a `std::string`?
        // After all, according to the C++ standard, `std::exception::what()`
        // returns a `const char *`; and this class receives a `const char *`
        // as its constructor argument.
        //
        // Those are both excellent questions, imaginary voice in my head!
        // The Socratic Method of your questions provides a convenient prompt
        // for me to document some important information that will help avoid
        // dangling pointers or memory leaks.
        //
        // And in fact, the answers to these 2 questions are closely connected.
        //
        // Answer #1: Note the following requirement upon the `const char *`
        // to be returned by `std::exception::what()`:
        //      """
        //      The pointer is guaranteed to be valid at least until the
        //      exception object from which it is obtained is destroyed,
        //      or until a non-const member function on the exception object
        //      is called.
        //      """
        //
        // So whatever error string is returned by `std::exception::what()`
        // must be stored *within* this exception instance for the remainder
        // of its lifetime (ie, as a RAII-lifetime data member).
        //
        // We can't just construct and return a new `const char *` from within
        // the `what()` method (whether a `std::string` or a `const char *`):
        //
        //  - temporary stack-based `std::string` in the function
        //      => use `std::string::c_str()` to get a `const char *`
        //      => `std::string` deallocated when function ends
        //      => dangling pointer (boo!).
        //
        //  - return a newly heap-allocated `const char *`
        //      => memory leak (boo!).
        //
        // And in fact, the cppreference docs for `std::ostringstream::str()`
        // (which returns a `std::string`) warn about this exact danger:
        //      """
        //      The copy of the underlying string returned by `str` is a
        //      temporary object that will be destructed at the end of the
        //      expression, so directly calling `c_str()` on the result of
        //      `str()` (for example in `auto *ptr = out.str().c_str();`)
        //      results in a dangling pointer.
        //      """
        //       -- https://en.cppreference.com/w/cpp/io/basic_ostringstream/str
        //
        // So instead, we create our own `std::string` copy of the supplied
        // `const char *`, so we can manage its lifetime using RAII.
        //
        // Answer #2: And of course (not that we ever need an excuse to use
        // `std::string` to hold our non-static strings), don't forget that
        // we have no control over the lifetime of the C-string provided to
        // the `const char *` constructor argument!  It might be a static
        // C-string (yay!), or it might be a soon-to-be-dangling-pointer
        // from some other `std::string::c_str()` (boo!).
        //
        // So this is why we hava a data member `m_what`,
        // and why we store a `std::string` rather than a `const char *`.
        const std::string m_what;
    };


    /*
     * This class is a C++ exception that is thrown in this C++ code,
     * corresponding to a specific function-call parameter argument value
     * that was passed in through the Python C-API.
     *
     * It extends our other C++ exception class `ValueError`,
     * with full Liskov Substitutability.
     *
     * This class extends `ValueError` with additional attributes
     * to indicate the specific function-call parameter that received
     * an erroneous argument.
     *
     * Like its C++ parent class `ValueError`, this C++ exception
     * will be converted to a Python `ValueError` exception and raised
     * in the Python code.
     */
    class ValueErrorForParam: public ValueError
    {
    public:
        ValueErrorForParam(
                const char *param_name,
                const char *param_descr,
                const char *err_descr):
            ValueError(construct_what(
                    param_name, param_descr, err_descr
            )),
            m_param_name(param_name),
            m_param_descr(param_descr),
            m_err_descr(err_descr) {  }

        ValueErrorForParam(
                const char *param_name,
                const char *param_descr,
                const std::string &err_descr):
            ValueError(construct_what(
                    param_name, param_descr, err_descr.c_str()
            )),
            m_param_name(param_name),
            m_param_descr(param_descr),
            m_err_descr(err_descr) {  }

        virtual
        ~ValueErrorForParam() {  }

        const std::string m_param_name;
        const std::string m_param_descr;
        const std::string m_err_descr;

    private:
        static
        const std::string
        construct_what(
                const char *param_name,
                const char *param_descr,
                const char *err_descr);
    };


    /*
     * This struct encapsulates the shape of a 2-D Numpy array.
     *
     * An instance of this struct should only be created in 2 circumstances:
     *
     *  1. It is a "valid" shape, meaning that it's a valid shape for a
     *   non-empty 2-D array, meaning that both `num_rows` (number of rows)
     *   & `num_cols` (number of columns) are positive integers.
     *
     *  2. It is an "empty" shape, meaning that both `num_rows` & `num_cols`
     *   are 0.  [This is used as a default-initialiser value, to indicate
     *   an invalid/uninitialised/unspecified shape.  This code was written
     *   for a pre-C++17 compiler, before `std::optional<T>` was added.]
     *
     * That is why this struct has a private constructor,
     * with a static factory/creation function for the "valid" shape,
     * and a singleton static data-member `NoShape` for the "empty" shape.
     *
     * This struct serves the following purposes:
     *
     *  * It aggregates the 2 integers of the shape, allowing the code to
     *    pass around one strongly-typed object rather than 2 weakly-typed
     *    integers (in code that is full of integers).
     *
     *  * It ensures that the shape is always in one of the 2 above-listed
     *    known states.
     *
     *  * It names the attributes `m_num_rows` & `m_num_cols` rather than
     *    simply "an anonymous integer" & "another anonymous integer".
     *
     *  * It ensures that when you compare two shape instances, you are
     *    comparing `num_rows` to `num_rows`, rather than potentially
     *    mixing-up your shape dimensions (`num_rows` vs `num_cols`).
     *
     * Sadly, mixing-up unnamed integer elements in a shape tuple seems to
     * be an all-too-common source of error (*cough* the channel dimension
     * in Numpy arrays vs PyTorch tensors *cough*).
     */
    struct Shape2d
    {
        size_t const m_num_rows, m_num_cols;

        // Static const instance `NoShape` for (0, 0).
        // Otherwise, all dimensions must be > 0.
        static const Shape2d NoShape;

        static
        const Shape2d
        make_valid(size_t num_rows, size_t num_cols);

    private:
        static
        const Shape2d
        make_no_shape();

        // Private constructor can only be called by static member functions
        // `make_valid(num_rows, num_cols)` or `make_no_shape()`.
        Shape2d(size_t num_rows, size_t num_cols):
            m_num_rows(num_rows), m_num_cols(num_cols) {  }
    };

    inline
    bool
    operator==(const Shape2d &a, const Shape2d &b)
    {
        return ((a.m_num_rows == b.m_num_rows) &&
                (a.m_num_cols == b.m_num_cols));
    }

    inline
    bool
    operator!=(const Shape2d &a, const Shape2d &b)
    {
        return ((a.m_num_rows != b.m_num_rows) ||
                (a.m_num_cols != b.m_num_cols));
    }

    std::ostream &
    operator<<(std::ostream &o, const Shape2d &s);


    /*
     * This class encapsulates the data & shape of a 2-D Numpy array
     * of C++ data-type `T`.
     *
     * An instance of this class should only be created when the `.dtype`
     * (a Numpy data-type) & the `.shape` (an arbitrary N-dim shape tuple)
     * of a caller-supplied Numpy array have been verified.
     *
     * Thus, this class serves multiple purposes:
     *
     *  1. It associates the array data with the shape in a single object,
     *    thus reducing the number of variables that must be passed around
     *    and tracked.
     *
     *  2. It indicates that the Numpy array's Numpy data-type has been
     *    verified as matching the C++ data-type `T`.
     *
     *  3. It indicates that the Numpy array shape has been verified as 2-D,
     *    thus consisting of (num_rows, num_cols).
     *
     *  4. It indicates that the (signed) integer values of num_rows & num_cols
     *    in a Numpy array have been verified to have positive values.
     *
     *    [Which obviously they should anyway, because it's nonsensical to
     *    have negative counts... But at an API boundary, there's no benefit
     *    in making assumptions that would really bite us if they were wrong.
     *    As the old saying goes: "A good programmer looks both ways before
     *    crossing a one-way street."  So let's double-check to be 100% sure
     *    at this API boundary.]
     *
     *  5. It offers optional array bounds-checking for array indexing.
     *
     *  6. It encapsulates the num_rows & num_cols as unsigned `size_t`, because
     *    unfortunately C++ array indexing expects an unsigned index type.
     */
    template<typename T>
    struct PyArray2d
    {
        typedef T data_type;

        // Note: For processing speed, our array-processing code
        // actually iterates element-by-element by pointer-increment.
        // So we actually need to allow access to the underlying pointer!
        // [I know, I'm as shocked/horrified/intrigued as you are.]
        //
        // Note that we don't allow *modification* of our pointer member.
        T *const m_data;

        const Shape2d m_shape;

        PyArray2d(T *data, const Shape2d &shape):
            m_data(data), m_shape(shape) {  }

        size_t
        num_elems() const
        {
            return m_shape.m_num_rows * m_shape.m_num_cols;
        }
    };

} // namespace PyArrayPP

#endif // PY_ARRAY_PP_HH

/*  
 * Vim modeline to expand tab characters to 4 spaces.
 * vim: et ts=4 sw=4
 */
