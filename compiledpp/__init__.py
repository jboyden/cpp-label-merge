from numpy import empty_like as np_empty_like

import _quantcolourlabelmerge


def merge_by_quant_colour(
        labels_to_merge, num_labels_to_merge,
        quant_colours, num_quant_colours,
        do_checks=True):

    # Numpy array `merged_labels_out` will be the return-value, in a pair-tuple
    # with the integer number of merged labels: `(labels, num_labels)`
    #
    # This Numpy array is of known shape & dtype, which is why it can be
    # allocated empty here and passed in through the C-API to be populated.
    #
    # In theory, if client code is concerned about the time-performance cost
    # (or memory consumption) of this array allocation, the client code can
    # call function `_quantcolourlabelmerge.merge_by_quant_colour` directly,
    # supplying its own pre-allocated, re-used Numpy array to avoid an
    # unnecessary memory allocation.
    merged_labels_out = np_empty_like(labels_to_merge)

    return (merged_labels_out,
            # number of merged labels:
            _quantcolourlabelmerge.merge_by_quant_colour(
                    labels_to_merge, num_labels_to_merge,
                    quant_colours, num_quant_colours,
                    merged_labels_out,
                    do_checks=do_checks))

