/*
 * PyArrayPP.cc:
 * Light-weight C++ wrappers around already-validated Numpy arrays.
 *
 * [Note: These are minimalist, lightweight C++ wrappers around Numpy arrays
 * via the Python C-API.  If you want something a little more comprehensive,
 * take a look at: https://github.com/jboy/nim-pymod ]
 *
 * This C++ source file includes the C++ header file `PyArrayPP.hh`.
 */

#include <cassert>  // assert
#include <ostream>  // std::ostream
#include <sstream>  // std::ostringstream

#include "PyArrayPP.hh"


/******************************************************************************
 *
 * Define static data-members, static member functions, and
 * non-trivial virtual member functions.
 *
 */


const std::string
PyArrayPP::ValueErrorForParam::construct_what(
        const char *param_name,
        const char *param_descr,
        const char *err_descr)
{
    // This code was written before `std::format` was available:
    //  https://en.cppreference.com/w/cpp/utility/format/format
    //
    // Of course, I would rather be using `std::format`...
    std::ostringstream oss;
    oss << param_descr
            << " (`" << param_name << "`) "
            << err_descr;

    // `std::ostringstream::str()` returns a `std::string`.
    //
    // Beware:
    //      """
    //      The copy of the underlying string returned by `str` is a
    //      temporary object that will be destructed at the end of the
    //      expression, so directly calling `c_str()` on the result of
    //      `str()` (for example in `auto *ptr = out.str().c_str();`)
    //      results in a dangling pointer.
    //      """
    //        -- https://en.cppreference.com/w/cpp/io/basic_ostringstream/str
    //
    // So, the `std::string` constructed in the `std::ostringstream` must be
    // stored within an exception instance for the remainder of its lifetime
    // (ie, as a data member) before we access the `c_str()`.
    //
    // And this is why we have a data member `m_what`.
    return oss.str();
}


const PyArrayPP::Shape2d PyArrayPP::Shape2d::NoShape =
        PyArrayPP::Shape2d::make_no_shape();


const PyArrayPP::Shape2d
PyArrayPP::Shape2d::make_valid(size_t num_rows, size_t num_cols)
{
    /*
     * These assertions check the correctness of caller-supplied values.
     *
     * To disable these runtime assertions at compile-time, either:
     *  `#define NDEBUG` on a line before `#include <assert.h>`
     * or compile with the G++ flag `-DNDEBUG`.
     */
    assert(num_rows > 0);
    assert(num_cols > 0);

    return Shape2d(num_rows, num_cols);
}


const PyArrayPP::Shape2d
PyArrayPP::Shape2d::make_no_shape()
{
    return Shape2d(0, 0);
}


std::ostream &
PyArrayPP::operator<<(std::ostream &o, const PyArrayPP::Shape2d &s)
{
    o << "Shape2d(" << s.m_num_rows << ", " << s.m_num_cols << ")";
    return o;
}


/*  
 * Vim modeline to expand tab characters to 4 spaces.
 * vim: et ts=4 sw=4
 */
