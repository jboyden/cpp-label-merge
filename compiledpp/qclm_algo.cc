/*
 * qclm_algo.cc (Quantized Colour Label Merge, algorithm implementation):
 * Merge region labels that overlap in quantized colour.
 *
 * This C++ source file includes the C++ header file `qclm_algo.hh`.
 */

#include <cassert>  /* assert */
#include <vector>

#include "qclm_algo.hh"


namespace QCLM
{

/******************************************************************************
 *
 * Type definitions & function prototype declarations.
 *
 */


/*
 * A simple facade for a `std::vector<T>` to behave like a 2-D array of `T`.
 */
template<typename T>
struct Array2d
{
    typedef T data_type;
    typedef typename std::vector<T>::iterator iter_type;

    Array2d(const PyArrayPP::Shape2d &shape, const T &init_val):
        m_num_cols(shape.m_num_cols),
        m_shape(shape),
        m_vector((shape.m_num_rows * shape.m_num_cols), init_val) {  }

    iter_type
    at(size_t r, size_t c)
    {
        assert (r >= 0 && r < m_shape.m_num_rows);
        assert (c >= 0 && c < m_shape.m_num_cols);
        size_t idx = r * m_num_cols + c;
        return m_vector.begin() + idx;
    }

private:
    const size_t m_num_cols;
    const PyArrayPP::Shape2d m_shape;
    std::vector<T> m_vector;
};


/*
 * Each `MergedLabelLink` instance is a link in a chain of merged labels.
 *
 * The `MergedLabelLink` type is basically a link in a singly-linked list
 * which ALSO contains a link back to the beginning of its own linked list.
 *
 * - Struct field `link_idx_of_first` is the link back to the beginning of
 *   its own linked list.
 * - Struct field `link_idx_of_next` is the link to the next link in its
 *   own linked list.
 *
 * There will be a non-empty container of type `std::vector<MergedLabelLink>`
 * (which we `typedef` to the alias type `Label_to_MergedLabelLink` below).
 * [Consult this `typedef` below for more documentation about this vector.]
 *
 * That vector will contain multiple `MergedLabelLink` instances, which may be
 * inter-woven into any number of independent singly-linked lists, depending on
 * how the links are connected to each other.  This weaving will be performed
 * in function `build_merger_graph`.
 *
 * When the vector is constructed, each & every `MergedLabelLink` instance
 * in the vector will be zero-initialised (using aggregate initialization;
 * see "Memory & initialization details" below in this docstring comment).
 *
 * So when the vector is first constructed, every `MergedLabelLink` instance
 * will technically be in an "invalid" state, believing that its "next link"
 * is the first link (i.e. the link at index 0).  [Except for this first link,
 * at index 0, which is in its own different invalid state because it believes
 * that it is its own "next link".]
 *
 * However, this "invalid" state is intentional: We use it to indicate whether
 * we have encountered a given unique label integer yet or not.  The relevant
 * snippet of code in function `build_merger_graph` looks like:
 *
 *      // Have we seen this label before?
 *      if (link_for_this_label->link_idx_of_first == 0) {
 *          // Nope, it's still initialised to the default of 0;
 *          // so we haven't seen this label ever before.
 *          // We'll initialise it to be its own merged label.
 *          link_for_this_label->link_idx_of_first = label;
 *          // etc...
 *
 * Function `build_merger_graph` is a long function because it handles cases
 * like needing to merge two singly-linked lists.
 *
 * Memory & initialization details:
 *
 * 2 fields of 4 bytes each => 8 bytes per `MergedLabelLink` instance.
 *
 * This struct `MergedLabelLink` should be "Plain Old Data"
 * (a.k.a, in C++11 and later, both "Trivial" & "Standard Layout").
 * Thus, it can be initialised using aggregate initialization:
 *  https://en.cppreference.com/w/cpp/language/aggregate_initialization
 */
struct MergedLabelLink
{
        /*
         * NOTE: Both of these must be *signed* integers, for the algorithm
         * (as implemented in function `merge_labels_by_graph`).
         *
         * Layout detail: `link_idx_of_first` should be the first field
         * defined in the struct, because it's the most-frequently accessed.
         * [A struct field offset of 0 can be optimised away.]
         */
        int32_t link_idx_of_first,
                link_idx_of_next;
};

/*
 * This vector will be initialised to the length `(num_labels_to_merge + 1)`
 * in function `merge_labels`.
 *
 * Then, in the subroutines, it will be used as an O(1) bijective lookup map
 * from label integers (used as indices) to `MergedLabelLink` instances.
 *
 * A "bijection" is a mapping "where each element of one set is paired with
 * exactly one element of the other set, and each element of the other set
 * is paired with exactly one element of the first set; there are no unpaired
 * elements between the two sets."
 *  -- https://en.wikipedia.org/wiki/Bijection
 *
 * So in this case, it's one label integer to one `MergedLabelLink`;
 * or equivalently, one `MergedLabelLink` for each unique label integer.
 *
 * This enables us to ask "For each unique label integer in the input labels,
 * to what new merged label integer should we map it (in the result labels)?"
 *
 * Note: Because you should never be able to have a negative count of anything,
 * the supplied count-of-unique-labels integer parameter `num_labels_to_merge`
 * should never be less than zero.  (Don't worry, we do actually double-check
 * this in the calling code!)
 *
 * So this verified `num_labels_to_merge` is declared of type `size_t`.
 * So the smallest possible value for `(num_labels_to_merge + 1)` is `1`.
 * So this vector will always be initialised to contain at least one element;
 * this vector will never be empty.
 */
typedef std::vector<MergedLabelLink> Label_to_MergedLabelLink;

// Note: The element type must be a *signed* integer, for the algorithm
// (as implemented in function `merge_labels_by_graph`).
typedef std::vector<int32_t> Label_to_signed_index;
typedef std::vector<int32_t> QuantColour_to_signed_index;


template<typename T>
inline
T *
operator+(std::vector<T> &vec, int32_t idx)
{
    assert (idx >= 0 && idx < static_cast<int32_t>(vec.size()));
    return & (vec[idx]);
}


template<typename T>
inline
T *
operator+(std::vector<T> &vec, size_t idx)
{
    assert (idx < vec.size());
    return & (vec[idx]);
}


void
build_merger_graph(
        Label_to_MergedLabelLink &links_per_label,
        Label_to_signed_index &link_idxes_of_last_by_label,
        QuantColour_to_signed_index &link_idxes_of_first_by_colour,
        const PyArrayPP::Shape2d &shape_labels_x_colours,
        const PyArrayPP::PyArray2d<int32_t> &labels_to_merge_2d,
        size_t num_labels_to_merge,
        const PyArrayPP::PyArray2d<int32_t> &quant_colours_2d,
        size_t num_quant_colours);


size_t
merge_labels_by_graph(
        Label_to_MergedLabelLink &links_per_label,
        Label_to_signed_index &merged_label_per_label_out,
        size_t num_labels_to_merge);


void
relabel_labels_to_merge(
        PyArrayPP::PyArray2d<int32_t> &merged_labels_out_2d,
        const PyArrayPP::PyArray2d<int32_t> &labels_to_merge_2d,
        size_t num_labels_to_merge,
        const Label_to_signed_index &merged_label_per_label);




/******************************************************************************
 *
 * Build the bidirectional graph of region labels & quantized colours to merge.
 * Then uh, merge it.
 *
 * We'll do a 2-D (per-pixel) pass through the region labels & quantized colours
 * to build the graph.
 *
 * Then we'll traverse the graph (node-by-node) to label it (and in so doing,
 * merge the labels).
 *
 */


size_t
merge_labels(
        PyArrayPP::PyArray2d<int32_t> &merged_labels_out_2d,
        const PyArrayPP::PyArray2d<int32_t> &labels_to_merge_2d,
        size_t num_labels_to_merge,
        const PyArrayPP::PyArray2d<int32_t> &quant_colours_2d,
        size_t num_quant_colours)
{
    /*
     * Algorithm design strategy:
     *
     *  - As described in detail in wrapper file `quantcolourlabelmerge.cc`,
     *    in the comment preceding C++ function `py_merge_by_quant_colour`,
     *    because the region labels are 8-connected, if the region labels
     *    are integer-contiguous (ie, without skipping over any integers),
     *    then the highest possible number of labels in `labels_to_merge_2d`
     *    is `(num_rows * num_cols) // 4`.
     *
     *  - Of course, it's NOT guaranteed that the region labels we've received
     *    ARE integer-contiguous (if the caller wants to be lazy or a jerk).
     *    So we must be able to handle that situation if it occurs.
     *
     *  - Likewise, it's VERY LIKELY that the number of quantized colours
     *    is less than half the number of pixels in the image (if we assume
     *    that, on average, every quantized colour occurs more than twice in
     *    the image).  But again, it's NOT guaranteed.  So we must be able to
     *    handle this situation if it occurs.
     *
     * We draw the following conclusion from these previous 3 statements:
     *
     * We will need to iterate over every pixel in `labels_to_merge_2d` &
     * `quant_colours_2d` to scan for connections and build linked-lists.
     *
     * But when we're merging these linked-lists, there will probably be
     * fewer computation steps overall if the linked-lists are per-label
     * and/or per-colour rather than per-pixel.  So we should construct
     * per-label and/or per-colour linked-lists rather than per-pixel.
     */

    // Add 1 to account for the zero-label (the background around objects).
    const size_t num_labels_to_merge_plus_1 = num_labels_to_merge + 1u;
    const size_t num_quant_colours_plus_1 = num_quant_colours + 1u;

    /*
     * This `Shape2d` is the shape of the Cartesion Product of
     * (number of unique intger labels) x (number of unique quantized colours)
     *
     * Later, in function `build_merger_graph`, we construct an `Array2d<bool>`
     * of this shape, to keep track of which (label, colour) pairings have been
     * seen.
     */
    const PyArrayPP::Shape2d shape_labels_x_colours =
            PyArrayPP::Shape2d::make_valid(
                    num_labels_to_merge_plus_1, num_quant_colours_plus_1);

    /*
     * We want to calculate a 1-D array (of length `(num_labels_to_merge + 1)`)
     * of type `int32_t`, so we can re-map the labels in `labels_to_merge_2d`
     * to the merged labels.
     *
     * We want the re-mapped labels to be "compressed" (ie, contained within
     * the smallest possible integer range, as close to 0, without any gaps).
     *
     * (Otherwise, we would need to re-"compress" these labels again anyway,
     * before we use them to calculate per-label means over the image.)
     *
     * Note: The (region labels <--> quantized colours) graph will in general
     * be many-to-many.
     */
    Label_to_MergedLabelLink links_per_label(
            num_labels_to_merge_plus_1, { 0, 0 });

    /*
     * We store the last-per-label outside of `MergedLabelLink`
     * (which contains the first-per-label & the per-link "next")
     * because the last-per-label is accessed much less frequently
     * than the other two.
     *
     * For each label-merger, exactly one last-per-label is read & updated.
     * [And there will always be `< num_labels_to_merge` mergers.]
     * In contrast, for each label-merger, in the later link-chain:
     * - Every per-link "next" will be read.
     * - Every per-link first-per-label will be updated.
     *
     * [Technically, the last-per-label is merely an optimisation.
     * We could calculate the last-per-label of the earlier link-chain,
     * simply by iterating through the links of the earlier link-chain;
     * storing the last-per-label simply caches it.]
     */
    Label_to_signed_index link_idxes_of_last_by_label(
            num_labels_to_merge_plus_1, 0);

    QuantColour_to_signed_index link_idxes_of_first_by_colour(
            num_quant_colours_plus_1, 0);

    build_merger_graph(
            links_per_label,
            link_idxes_of_last_by_label,
            link_idxes_of_first_by_colour,
            shape_labels_x_colours,
            labels_to_merge_2d, num_labels_to_merge,
            quant_colours_2d, num_quant_colours);

    size_t num_merged_labels =
            merge_labels_by_graph(
                    links_per_label,
                    link_idxes_of_last_by_label,
                    num_labels_to_merge);

    relabel_labels_to_merge(
            merged_labels_out_2d,
            labels_to_merge_2d, num_labels_to_merge,
            link_idxes_of_last_by_label);

    return num_merged_labels;
}


/*
 * Build the graph of label-mergers in vector `links_per_label`.
 *
 * The following 3 pre-allocated, zero-initialised vectors are populated
 * with cross-linking indices by setting their integer element values:
 *
 * - `links_per_label`
 * - `link_idxes_of_last_by_label`
 * - `link_idxes_of_first_by_colour`
 *
 * This function `build_merger_graph` is the most complex function,
 * performing the most important (and complex!) task of our algorithm.
 * It contains many long comments, to record our reasoning & assumptions
 * about the state of the system at every step.
 *
 * This function has a theoretical worst-case of O(P*L) in computation,
 * and a theoretical best-case of O(P) in computation, for:
 *
 * - P := the number of pixels in the 2-D array;
 * - L := the number of labels (unique label integers; always L < P),
 *
 * due to the pseudocode:
 *
 *      for each p in pixels:
 *          process `p`  # Examine its label & quantized colour.
 *          if a label-merge is necessary:
 *              for each link in latter chain of newly-merged link-chain:
 *                  update `link->link_idx_of_first` to point to earlier chain
 *
 * Obviously a factor of O(P) is unavoidable in a function that processes
 * all pixels in an array...
 *
 * Whether the algorithm encounters its best-case or worst-case depends on
 * the order in which labels-to-merge are encountered:
 *
 * - Once two labels have been merged into a single chain, they can never
 *   be merged with each other again.  There will only be `(L - 1)` merges,
 *   regardless of how many pixels `P` we visit.
 *
 * - Conversely, we will always visit `P` pixels, regardless of whether
 *   a merge is needed at any given pixel.
 *
 * - Each label-merge is best-case O(1) and worst-case O(L).
 *
 * - The best-case occurs if, for all label-merges, the latter chain always
 *   has length 1 (leading to a possible O(1) at every pixel), with all other
 *   chain-links in the earlier chain.
 *
 * - The worst-case occurs if, for all label-merges, the earlier chain always
 *   has length 1, with all other chain-links in the latter chain (leading to
 *   a possible O(L) at every pixel).
 *
 * So this algorithm has the best-possible best case, but the worst-case is
 * not wonderful.
 */
void
build_merger_graph(
        Label_to_MergedLabelLink &links_per_label,
        Label_to_signed_index &link_idxes_of_last_by_label,
        QuantColour_to_signed_index &link_idxes_of_first_by_colour,
        const PyArrayPP::Shape2d &shape_labels_x_colours,
        const PyArrayPP::PyArray2d<int32_t> &labels_to_merge_2d,
        size_t num_labels_to_merge,
        const PyArrayPP::PyArray2d<int32_t> &quant_colours_2d,
        size_t num_quant_colours)
{
    // Silence the warning about
    // "unused parameter `num_labels_to_merge` [-Wunused-parameter]".
    // and likewise for `num_quant_colours`.
    (void)num_labels_to_merge;
    (void)num_quant_colours;

    // Keep track of which (label, colour) pairings have been seen.
    Array2d<bool> is_seen_labels_x_colours(shape_labels_x_colours, false);

    // We've already verified that `labels_to_merge_2d` & `quant_colours_2d`
    // have the same shape, and are both C-contiguous -- for example:
    assert(labels_to_merge_2d.m_shape == quant_colours_2d.m_shape);

    // So we can iterate through them both, pixel-by-pixel, at the same time.
    // And in fact, we can iterate through them in 1-D as "flattened" arrays
    // of `num_pixels := num_rows * num_cols` pixels, where the hypothetical
    // 1-D index traverses the half-open range `[0, num_pixels)`.
    const size_t num_pixels = labels_to_merge_2d.num_elems();
    int32_t *labels_to_merge_iter = labels_to_merge_2d.m_data;
    int32_t *labels_to_merge_end = labels_to_merge_2d.m_data + num_pixels;
    int32_t *quant_colours_iter = quant_colours_2d.m_data;

    for ( ; labels_to_merge_iter != labels_to_merge_end;
            ++labels_to_merge_iter, ++quant_colours_iter) {

        // Every pixel has a region label & a colour (although it might be 0).
        int32_t label = *labels_to_merge_iter;
        int32_t colour = *quant_colours_iter;

        /*
         * These assertions check the correctness of caller-supplied values.
         *
         * To disable these runtime assertions at compile-time, either:
         *  `#define NDEBUG` on a line before `#include <assert.h>`
         * or compile with the G++ flag `-DNDEBUG`.
         */
        assert(label >= 0);
        assert(label <= static_cast<int32_t>(num_labels_to_merge));
        assert(colour >= 0);
        assert(colour <= static_cast<int32_t>(num_quant_colours));

        // Ignore `label == 0` or `colour == 0`.
        if (label == 0 || colour == 0) continue;

        // If the region label & quantized colour are NOT 0 at this pixel,
        // check whether we've seen this (label, colour) pairing before.
        Array2d<bool>::iter_type is_already_seen_label_colour_pairing =
                is_seen_labels_x_colours.at(label, colour);
        if (*is_already_seen_label_colour_pairing) continue;
        else {
            // This (label, colour) pairing has NOT yet been seen.
            // So we must process this (label, colour) pairing.

            // This variable must be of type `int32_t` because we assign
            // `link_for_this_label->link_idx_of_first` to it.
            int32_t link_idx_of_first_for_this_label;
            // This variable must be of type `int32_t` because
            // we assign `link_idxes_of_first_by_colour[colour]` to it;
            // and the element-type of vector `link_idxes_of_first_by_colour`
            // must be `int32_t` because
            // we assign `link_idx_of_first_for_this_label` to it.
            int32_t link_idx_of_first_for_this_colour;

            // First, we'll work out the link-index of the first link
            // in the merged-label link-chain by label & colour.
            {
                MergedLabelLink *link_for_this_label = links_per_label + label;

                // Before we continue (and before we forget):
                // Mark this (label, colour) pairing as now "already seen".
                (*is_already_seen_label_colour_pairing) = true;

                // Have we seen this label before?
                if (link_for_this_label->link_idx_of_first == 0) {
                    // Nope, it's still initialised to the default of 0;
                    // so we haven't seen this label ever before.
                    // We'll initialise it to be its own merged label.
                    link_for_this_label->link_idx_of_first = label;
                    link_idxes_of_last_by_label[label] = label;
                }
                link_idx_of_first_for_this_label = link_for_this_label->link_idx_of_first;

                // Have we seen this colour before?
                if (link_idxes_of_first_by_colour[colour] == 0) {
                    // Nope, it's still initialised to the default of 0;
                    // so we haven't seen this colour ever before.
                    // We'll initialise it to point to the appropriate
                    // merged label.
                    link_idxes_of_first_by_colour[colour] = link_idx_of_first_for_this_label;
                    // Because we have never seen this colour before,
                    // there's no chance of it causing a colour-overlap of
                    // two different labels.  So there's no more checking
                    // we need to do for this pixel.
                    continue;
                }
                link_idx_of_first_for_this_colour = link_idxes_of_first_by_colour[colour];
            }

            /*
             * If we got to here, we HAVE seen this colour before
             * (and `link_idxes_of_first_by_colour[colour]` had
             * a non-zero value already from some previous pixel,
             * in a previous pass through this loop).
             *
             * We MAY or MAY NOT have seen this label before.
             * We have NOT seen this (label, colour) pairing before.
             * We need to check whether there's any need for a label-merger.
             *
             * NOTE: It's not at all surprising if:
             *
             *      label != link_idx_of_first_for_this_colour
             *
             * because `label` might just be mid-way through some link-chain.
             *
             * But, if:
             *
             *      link_idx_of_first_for_this_label != link_idx_of_first_for_this_colour
             *
             * then we have a special case, possibly requiring a label-merger.
             */
            if (link_idx_of_first_for_this_label != link_idx_of_first_for_this_colour) {
                /*
                 * Ah, the value of `link_idx_of_first_for_this_colour` here
                 * is what we described above in the comment immediately above:
                 *
                 * We *might* need to do a label-merger.
                 *
                 * We assume there are two possible special case situations:
                 *
                 *  1. `link_idxes_of_first_by_colour[colour]` is out-of-date
                 *    after a merger, and it indicates a link mid-way through
                 *    some longer link-chain of merged labels.
                 *
                 *  2. This colour has just been seen with a new label in a
                 *    new merged-label link-chain, so we need to perform a
                 *    label-merger.
                 *
                 * (QUESTION:  Could we encounter situations #1 & #2 at
                 * the same time?  I think possibly so...)
                 *
                 * But we assume that the following 4 statements are always
                 * valid:
                 *
                 *  3. Each link is correctly aware of which link-chain it is
                 *    a part of (as identified by `link_idx_of_first`).
                 *
                 *  4. Every link-chain (whether consisting of a single link
                 *    or multiple links) is in a correct internal state
                 *    (ie, `link_idx_of_next` are correct for all links).
                 *
                 *  5. An element `link_idxes_of_last_by_label[label]` is
                 *    up-to-date whenever `label` corresponds to the first
                 *    link of a link-chain (ie, whenever `label` is equal
                 *    to `links_per_label[some_label].link_idx_of_first`).
                 *    (We won't bother to keep any other elements in this
                 *    array up-to-date.)
                 *
                 *  6. Even if element `link_idxes_of_first_by_colour[colour]`
                 *    is out-of-date for some `colour`, that element still
                 *    indicates a `MergedLabelLink` for a label for which
                 *    there has been a valid (label, colour) pairing with
                 *    this `colour`.  And even if that label merges with
                 *    other labels (and thus, its `MergedLabelLink` becomes
                 *    part of a larger link-chain), this (label, colour)
                 *    association will remain valid.
                 *
                 * Special case #1 occurs when the link-chain indicated by
                 * `link_idxes_of_first_by_colour[colour]` is merged into
                 * a new longer link-chain as the latter sub-chain.  There's
                 * no way to update `link_idxes_of_first_by_colour[colour]`
                 * directly when this label-merger happens!
                 *
                 * But by assumption #3, even if the link indicated by
                 * `link_idxes_of_first_by_colour[colour]` is mid-way through
                 * a longer link-chain, at least this link will correctly know
                 * its own `link_idx_of_first`.  So we could simply use this to
                 * update the value of `link_idxes_of_first_by_colour[colour]`.
                 *
                 * But if
                 * `link_idxes_of_first_by_colour[colour]->link_idx_of_first`
                 * is *still* not equal to `link_idx_of_first_for_this_label`,
                 * then this means that `link_idxes_of_first_by_colour[colour]`
                 * is in a different link-chain:  We have disjoint chains.
                 * This is the situation in which we must do a label-merger.
                 *
                 * Either way (and at the very least), we'll need to update the
                 * value of `link_idxes_of_first_by_colour[colour]` afterwards.
                 */

                // Now, we update `link_idx_of_first_for_this_colour` to be
                // *actually* the value of `link_idx_of_first` for the link
                // indicated by `link_idxes_of_first_by_colour[colour]`,
                // rather than what
                // the out-of-date `link_idxes_of_first_by_colour[colour]`
                // *thinks* the index of the first link in the link-chain is.
                {
                    MergedLabelLink *first_link_for_this_colour =
                            (links_per_label + link_idx_of_first_for_this_colour);
                    link_idx_of_first_for_this_colour =
                            first_link_for_this_colour->link_idx_of_first;
                }
                if (link_idx_of_first_for_this_label != link_idx_of_first_for_this_colour) {
                    /*
                     * The two `link_idx_of_first` values are still not equal.
                     * This must mean that we have disjoint chains.
                     * So we must do a label-merger.
                     *
                     * The code in this `if`-block is a label-merger.
                     * It must occur quite infrequently, because there are
                     * `num_labels_to_merge` labels, so it is only possible
                     * for `(num_labels_to_merge - 1)` label-mergers to occur.
                     *
                     * We'll merge the two chains so that whichever chain
                     * has the lesser `link->link_idx_of_first` will be
                     * earlier in the merged chain.
                     */
                    MergedLabelLink *earlier_chain, *later_chain;
                    {
                        MergedLabelLink *first_link_for_this_label =
                                (links_per_label + link_idx_of_first_for_this_label);
                        MergedLabelLink *first_link_for_this_colour =
                                (links_per_label + link_idx_of_first_for_this_colour);

                        if (link_idx_of_first_for_this_label < link_idx_of_first_for_this_colour) {
                            earlier_chain = first_link_for_this_label;
                            later_chain = first_link_for_this_colour;
                        } else {
                            // (link_idx_of_first_for_this_colour < link_idx_of_first_for_this_label)
                            earlier_chain = first_link_for_this_colour;
                            later_chain = first_link_for_this_label;
                        }
                    }
                    link_idx_of_first_for_this_label = earlier_chain->link_idx_of_first;
                    // Now `link_idx_of_first_for_this_label` is the link-index
                    // of the first link for this *merged* label.

                    // Link the two chains by the "next" link between them:
                    //    earlier->last->next = later->first;
                    {
                        int32_t link_idx_of_last_of_earlier_chain =
                                link_idxes_of_last_by_label[link_idx_of_first_for_this_label];
                        MergedLabelLink *last_link_of_earlier_chain =
                                (links_per_label + link_idx_of_last_of_earlier_chain);
                        last_link_of_earlier_chain->link_idx_of_next = later_chain->link_idx_of_first;
                    }
                    // Update `link_idxes_of_last_by_label`
                    // for the first link of this new merged chain.
                    {
                        int32_t link_idx_of_first_of_later_chain = later_chain->link_idx_of_first;
                        link_idxes_of_last_by_label[link_idx_of_first_for_this_label] =
                                link_idxes_of_last_by_label[link_idx_of_first_of_later_chain];
                    }
                    // Now iterate through all links in the latter chain,
                    // updating `link_idx_of_first` for this new merged chain.
                    {
                        int32_t link_idx_of_first_of_later_chain = later_chain->link_idx_of_first;
                        int32_t link_idx_of_next;
                        MergedLabelLink *iter_link = links_per_label + link_idx_of_first_of_later_chain;
                        while (true) {
                            iter_link->link_idx_of_first = link_idx_of_first_for_this_label;

                            link_idx_of_next = iter_link->link_idx_of_next;
                            if (link_idx_of_next == 0) {
                                // We've reached the end of the link-chain.
                                break;
                            } else {
                                // Advance to the next link in this chain.
                                iter_link = links_per_label + link_idx_of_next;
                            }
                        }
                    }
                }

                // Finally, update `link_idxes_of_first_by_colour[colour]`
                // and then we're done for this pixel.
                link_idxes_of_first_by_colour[colour] = link_idx_of_first_for_this_label;
            }
        }
    }
}


/*
 * Use the graph of label-mergers to relabel the link-chains.
 *
 * The graph of label-mergers was constructed in function `build_merger_graph`.
 * That function populated the index values in `links_per_label`.
 *
 * Now in this function, we will modify the links in `links_per_label`
 * with new "merged labels".
 *
 * As we do this, we will also create a surjective ("many-to-one") mapping
 * of (input) labels to merged labels in vector `merged_label_per_label_out`.
 *
 * [This mapping will probably NOT be injective ("one-to-one"), because it is
 * our intention to merge labels!  And we certainly intend it to be surjective,
 * because we don't want "gaps" between label integers in the merged labels.]
 *
 * To enable us to implement an O(L) algorithm, for L := the number of labels
 * (i.e. to enable us to perform a "single pass" through the label-mappings),
 * rather than an O(L^2) algorithm, we perform some tricksy hobbitses tricks:
 *
 * - We count (keep track of) the new merged labels *backwards* from zero
 *   (ie, in the negative integers), so that we know whether a label integer
 *   is an input label (positive) or a new merged label (negative).
 *
 *   [Fun fact the first: This enables us to handle/ignore missing input label
 *   integers (ie, "gaps" between label integers in the input labels), which
 *   show up as links with `link_idx_of_first` still initialized to zero.]
 *
 *   [Fun fact the second: This use of *negative* merged label integers is
 *   why all our link indices are signed `int32_t` integer types rather than
 *   unsigned `uint32_t` types.]
 *
 * - When we set a new merged label in a `MergedLabelLink`, we also jump
 *   forward *just once* to the "next" `MergedLabelLink`, to set the same
 *   new merged label in that one.
 *
 *   Because we jump forward just once per `MergedLabelLink` (in contrast to
 *   iterating through its whole linked list), our algorithm remains O(L)
 *   (in contrast to the O(L^2) of multiple linked-list iterations within
 *   an overall iteration).
 *
 * - As we go, we also store the *negative* of the (negative) merged label
 *   in `merged_label_per_label_out` (ie, it contains non-negative integers).
 *
 * After this, the final subroutine function `relabel_labels_to_merge` is a
 * very straightforward "remap values using values from a 1-D mapping array"
 * operation, the sort of thing you accomplish in a single Numpy expression:
 *
 *      resulting_remapped_values = value_mapping_1d[input_values]
 */
size_t
merge_labels_by_graph(
        Label_to_MergedLabelLink &links_per_label,
        Label_to_signed_index &merged_label_per_label_out,
        size_t num_labels_to_merge)
{
    /*
     * We want to label the merged label link-chains with
     * a new merged label integer.
     *
     * We want to do this in one pass: O(L) rather than O(L^2).
     * So we'll iterate through `links_per_label`.
     * At each link, we'll examine the `link_idx_of_first`:
     *
     * - If it's zero, then the link-index still has its initial value,
     *   so it was never processed.
     *   - Ignore it.
     *
     * - If it's positive, then we haven't yet processed this link,
     *   and it is a valid MergedLabelLink.
     *   - Decrement `curr_merged_label` to get a new unique negative
     *     merged label integer.
     *   - Set `link_idx_of_first` to this negative merged label integer.
     *   - If it has a non-zero "next" link-index, jump ahead just once
     *     to that link, to set its `link_idx_of_first` to this same
     *     negative merged label integer.  Don't go ahead any further
     *     or we'll end up with O(L^2).
     *
     * - Get back to the current link's `link_idx_of_first`.
     *   - If it's already negative, then we've already processed this link
     *     (from a previous "next" link).
     *   - But if it has a non-zero "next" link-index, then again jump ahead
     *     just once to set the *next* link.
     */

    MergedLabelLink *links_per_label_iter = links_per_label + 0;

    int32_t curr_merged_label = 0;
    auto merged_label_per_label_iter = merged_label_per_label_out.begin();
    auto merged_label_per_label_end = merged_label_per_label_out.end();
    assert(merged_label_per_label_out.size() == num_labels_to_merge + 1);

    for ( ; merged_label_per_label_iter != merged_label_per_label_end;
            ++links_per_label_iter, ++merged_label_per_label_iter) {

        int32_t link_idx_of_first = links_per_label_iter->link_idx_of_first;
        if (link_idx_of_first == 0) {
            /*
             * The link-index-of-first still has its initial value of 0,
             * so this MergedLabelLink was never processed.
             * That means there was never a label in the 2-D labels array
             * that indexed to this MergedLabelLink in `links_per_label`.
             * For the same reason, the corresponding element (indexed by
             * the same label) in array array `merged_label_per_label_out`
             * would also never have been processed, so it would also still
             * have its initial value of 0.  So there's no need to update
             * its value.
             *
             * (But we WILL verify this assumption with an assertion,
             * to validate the correctness of this module's code.)
             */

            /*
             * This assertion checks the correctness of this module's code.
             *
             * To disable these runtime assertions at compile-time, either:
             *  `#define NDEBUG` on a line before `#include <assert.h>`
             * or compile with the G++ flag `-DNDEBUG`.
             */
            assert((*merged_label_per_label_iter) == 0);

            continue;
        } else {
            int32_t merged_label_to_use;
            if (link_idx_of_first > 0) {
                // If it's positive, we haven't yet processed this link,
                // and it is a valid MergedLabelLink.
                merged_label_to_use = --curr_merged_label;
                links_per_label_iter->link_idx_of_first = merged_label_to_use;
                *merged_label_per_label_iter = -merged_label_to_use;
            } else {  // (link_idx_of_first < 0)
                merged_label_to_use = links_per_label_iter->link_idx_of_first;
            }
            int32_t link_idx_of_next = links_per_label_iter->link_idx_of_next;
            /*
             * This assertion checks the correctness of this module's code.
             *
             * To disable these runtime assertions at compile-time, either:
             *  `#define NDEBUG` on a line before `#include <assert.h>`
             * or compile with the G++ flag `-DNDEBUG`.
             */
            assert(link_idx_of_next <= static_cast<int32_t>(num_labels_to_merge));
            if (link_idx_of_next > 0) {
                // This link has a "next" link.
                (links_per_label + link_idx_of_next)->link_idx_of_first = merged_label_to_use;
                *(merged_label_per_label_out + link_idx_of_next) = -merged_label_to_use;
            }
        }
    }
    /*
     * By this point, `merged_label_per_label_out` should have been relabeled
     * with the (positive) merged labels.
     *
     * The variable `curr_merged_label` now contains the *negative* of the
     * highest (positive) unique label value.
     */
    assert(curr_merged_label <= 0);
    return static_cast<size_t>(-curr_merged_label);
}


/*
 * Remap values in `labels_to_merge_2d` using `merged_label_per_label`.
 *
 * Store the result of the value-remapping into `merged_labels_out_2d`.
 *
 * This is a very straightforward
 * "remap values using values from a 1-D mapping array" operation,
 * the sort of thing you accomplish in a single Numpy expression:
 *
 *      resulting_remapped_values = value_mapping_1d[input_values]
 *
 * or more specifically in this case:
 *
 *      merged_labels_out_2d =  merged_label_per_label[labels_to_merge_2d]
 *
 * This function is obviously unavoidably O(P) in computation,
 * for P := the number of pixels in the 2-D array.
 */
void
relabel_labels_to_merge(
        PyArrayPP::PyArray2d<int32_t> &merged_labels_out_2d,
        const PyArrayPP::PyArray2d<int32_t> &labels_to_merge_2d,
        size_t num_labels_to_merge,
        const Label_to_signed_index &merged_label_per_label)
{
    // Silence the warning about
    // "unused parameter `num_labels_to_merge` [-Wunused-parameter]".
    (void)num_labels_to_merge;

    /*
     * Step 1:
     * We project the merged labels from `labels_to_merge_2d`
     * into `merged_labels_out_2d`.
     *
     * Step 2:
     * We iterate pixel-by-pixel through `labels_to_merge_2d`
     * and `merged_labels_out_2d` together.
     */

    // We've already verified that `labels_to_merge_2d` & `merged_labels_out_2d`
    // have the same shape, and are both C-contiguous -- for example:
    assert(labels_to_merge_2d.m_shape == merged_labels_out_2d.m_shape);

    // So we can iterate through them both, pixel-by-pixel, at the same time.
    // And in fact, we can iterate through them in 1-D as "flattened" arrays
    // of `num_pixels := num_rows * num_cols` pixels, where the hypothetical
    // 1-D index traverses the half-open range `[0, num_pixels)`.
    const size_t num_pixels = labels_to_merge_2d.num_elems();
    int32_t *labels_to_merge_iter = labels_to_merge_2d.m_data;
    int32_t *labels_to_merge_end = labels_to_merge_2d.m_data + num_pixels;
    int32_t *merged_labels_out_iter = merged_labels_out_2d.m_data;

    for ( ; labels_to_merge_iter != labels_to_merge_end;
            ++labels_to_merge_iter, ++merged_labels_out_iter) {

        int32_t label = *labels_to_merge_iter;
        int32_t merged_label = merged_label_per_label[label];
        *merged_labels_out_iter = merged_label;

        /*
         * These assertions check the correctness of this module's code.
         *
         * To disable these runtime assertions at compile-time, either:
         *  `#define NDEBUG` on a line before `#include <assert.h>`
         * or compile with the G++ flag `-DNDEBUG`.
         */
        assert(label >= 0);
        assert(label <= static_cast<int32_t>(num_labels_to_merge));
        assert(merged_label >= 0);
        assert(merged_label <= static_cast<int32_t>(num_labels_to_merge));
    }
    // By this point, `merged_labels_out` should have been relabeled with
    // the (positive) merged labels.
}


} // namespace QCLM

/*  
 * Vim modeline to expand tab characters to 4 spaces.
 * vim: et ts=4 sw=4
 */
