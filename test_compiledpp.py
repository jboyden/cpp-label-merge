#!/usr/bin/env python
#
# A tiny Python script to demo our compiled C++ Python C-API module.
#
# Usage:
#   python test_compiledpp.py
#
# What this tiny Python script does:
#  1. Load the accompanying test image.
#  2. Label the "smooth regions" in the image
#     (performing the world's simplest image segmentation).
#  3. Quantize the colours in the image
#     (performing the world's simplest colour quantization).
#  4. Use our fancy home-grown label-merging algorithm implemented in C++
#     (called via function `merge_by_quant_colour` in package `compiledpp`)
#     to merge by labels & quantized colours.
#  5. Plot the results using Matplotlib.
#
# Our label-merging algorithm is both correct & fast, but we usually give it
# slightly more advanced inputs than the extremely-simple segmentation labels
# &d quantized colours we use here.

import matplotlib.figure as fig
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr

from scipy.misc import imread
from scipy.ndimage.filters import sobel as ndi_sobel
from scipy.ndimage.measurements import label as ndi_label

from compiledpp import merge_by_quant_colour


_PLOT_RESULTS = True


# Source of test image:
#  https://en.wikipedia.org/wiki/File:SIPI_Jelly_Beans_4.1.07.tiff
#       """
#       Source: SIPI Image Database
#       Picture of jelly beans taken at USC. Free to use.
#       Image released for use as a Standard Test Image.
#       """
#
# Linked from:
#  https://en.wikipedia.org/wiki/Standard_test_image
_TEST_IMG_FNAME = "SIPI_Jelly_Beans_4.1.07.png"

# Seed the random-number generator for reproducability.
_RAND_SEED = 42


def main():
    # Seed the random-number generator for reproducability.
    npr.seed(_RAND_SEED)

    (test_img_uint8, test_img) = read_3_chan_img(_TEST_IMG_FNAME)
    (smooth_region_labels, num_labels) = label_smooth_regions(test_img)
    (quantized_colours_3_chans, quantized_colours, num_quantized_colours) = \
            quantize_colours(test_img)

    (merged_labels, num_merged_labels) = \
            merge_by_quant_colour(
                    smooth_region_labels, num_labels,
                    quantized_colours, num_quantized_colours,
                    do_checks=True)

    if _PLOT_RESULTS:
        subplots_to_add = [
                (_TEST_IMG_FNAME,           test_img_uint8),
                ("Smooth Region Labels",    permute_labels(
                                                    smooth_region_labels,
                                                    num_labels)
                                            ),
                ("Quantized Colours",       quantized_colours_3_chans),
                ("Merged Colours & Labels", merged_labels),
        ]

        plot_title = "Matplotlib: C++ Label Merge: test_compiledpp.py"
        fig_subplot_params = fig.SubplotParams(
                left=0.05, bottom=0.03, right=0.99, top=0.92)
        figure = plt.figure(plot_title,  # This sets the Xorg window title.
                frameon=False, subplotpars=fig_subplot_params)
        figure.suptitle(plot_title)  # This sets a title on the plot canvas.

        for subplot_idx, (subtitle, img_to_show) in enumerate(subplots_to_add):
            ax = figure.add_subplot(2, 2, subplot_idx + 1)
            ax.imshow(img_to_show)
            ax.set_title(subtitle)

        plt.show()


def read_3_chan_img(img_fname, show_plots=False):
    """Read a 3-channel image specified by filename `img_fname`.

    Assume that the standard image-reading function returns a uint8 [0, 255];
    convert this to a float32 [0.0, 1.0].

    Return a 2-tuple (uint8 image, float32 image).
    """
    img_uint8 = imread(img_fname)  # Every component is now [0, 255].
    assert img_uint8.dtype == np.uint8  # Sanity check
    if img_uint8.shape[2] > 3:  # There is an alpha channel.
        # Strip the alpha channel.
        img_uint8 = img_uint8[:,:,:3]

    if show_plots:
        plt.imshow(img_uint8)
        plt.show()

    # Convert the component ranges from [0, 255] to [0.0, 1.0].
    img_float32 = img_uint8.astype(np.float32)
    img_float32 *= np.float32(1.0/255.0)  # Every component is now [0.0, 1.0].

    return (img_uint8, img_float32)


def label_smooth_regions(img, show_plots=False, labels_dtype=np.int32):
    """Label smooth regions in `img`.

    Hooray for the world's simplest superpixel algorithm!
    (Even simpler than SLIC!)
    """
    grad_mag_sqrd = calc_grad_mag_sqrd(img)
    if show_plots:
        plt.imshow(grad_mag_sqrd)
        plt.show()

    smooth_regions = (grad_mag_sqrd < grad_mag_sqrd.mean())
    if show_plots:
        plt.imshow(smooth_regions)
        plt.show()

    # Label the smooth regions.
    (labels, num_labels) = ndi_label(smooth_regions, output=labels_dtype)
    if show_plots:
        plt.imshow(labels)
        plt.show()

    return (labels, num_labels)


def calc_grad_mag_sqrd(img, dtype=np.float32):
    """Calculate a simple gradient-magnitude squared at each pixel."""
    (num_rows, num_cols, num_chans) = img.shape
    grad_mag_sqrd = np.zeros((num_rows, num_cols), dtype=dtype)
    tmp_sobel_out = np.empty((num_rows, num_cols), dtype=dtype)
    for chan_idx in range(num_chans):
        chan = img[:,:,chan_idx]

        # gradient delta-row for this channel
        ndi_sobel(chan, axis=0, output=tmp_sobel_out)
        tmp_sobel_out *= tmp_sobel_out  # Square the delta-row values.
        grad_mag_sqrd += tmp_sobel_out

        # gradient delta-col for this channel
        ndi_sobel(chan, axis=1, output=tmp_sobel_out)
        tmp_sobel_out *= tmp_sobel_out  # Square the delta-col values.
        grad_mag_sqrd += tmp_sobel_out

    return grad_mag_sqrd


def quantize_colours(img, show_plots=False, dtype=np.int32):
    """Quantize the float32 RGB colours in `img` into 27 ( == 3*3*3) colours.

    Hooray for the world's simplest colour-quantization algorithm!
    (Even simpler than K-means clustering!)
    """
    num_levels_per_chan = 3

    (num_rows, num_cols, num_chans) = img.shape
    quantized_3_chans = np.empty_like(img)
    quantized_1_chan = np.zeros((num_rows, num_cols), dtype=dtype)
    is_greater = np.empty((num_rows, num_cols), dtype=np.bool)
    for chan_idx in range(num_chans):
        chan = img[:,:,chan_idx]

        chan_min = chan.min()
        chan_max = chan.max()
        chan_range = chan_max - chan_min
        quant_range = chan_range / float(num_levels_per_chan)

        lower_thresh = chan_min + quant_range
        upper_thresh = lower_thresh + quant_range

        # Multiply the previous `quantized_1_chan` result
        # (from the previous channels) by `num_levels_per_chan`.
        quantized_1_chan *= num_levels_per_chan

        np.greater(chan, lower_thresh, out=is_greater)
        # Using `.view` to alias `bool` as `int8` avoids a copy.
        quantized_1_chan += is_greater.view(np.int8)
        quantized_3_chans[:,:,chan_idx] = is_greater.view(np.int8)

        np.greater(chan, upper_thresh, out=is_greater)
        # Using `.view` to alias `bool` as `int8` avoids a copy.
        quantized_1_chan += is_greater.view(np.int8)
        quantized_3_chans[:,:,chan_idx] += is_greater.view(np.int8)

    quantized_3_chans /= float(num_levels_per_chan)

    if show_plots:
        plt.imshow(img)
        plt.show()
        plt.imshow(quantized_3_chans)
        plt.show()
        plt.imshow(quantized_1_chan)
        plt.show()

    return (quantized_3_chans, quantized_1_chan, num_levels_per_chan ** 3)


def permute_labels(labels, num_labels):
    """Randomly permute the labels, so they stand out more clearly."""
    # We want the value of 0 to remain 0 after the permutation.
    perm_map_offset_1 = npr.permutation(num_labels) + 1
    # In fact, for visualisation purposes,
    # let's put some visual (i.e. colour) distance
    # between the value 0 and the non-zero values.
    perm_map_offset_1 += (num_labels // 4)

    # We need `perm_map` to be large enough to handle the N elements in
    # `perm_map_offset_1`, plus an element for the value of 0.
    perm_map = np.empty(num_labels + 1, dtype=np.int32)
    perm_map[0] = 0
    perm_map[1:] = perm_map_offset_1

    return perm_map[labels]


if __name__ == "__main__":
    main()
